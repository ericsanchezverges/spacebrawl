﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitOnClick : MonoBehaviour {

	public void ExitGame(){
	#if UNITY_DIRECTOR
		UnityEditor.EditorApplication.isPlaying = false;
	#else 
		Application.Quit ();
	#endif
	}
}
