﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healthBar : MonoBehaviour {

	float lifePoint;
	float Pos100;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		GetLife();
	}

	void GetLife(){
		GameObject findLife = GameObject.FindWithTag("Player");
       	p_type life = findLife.GetComponent<p_type>();
		lifePoint = life.playerHP;
		if(lifePoint <= 100 && lifePoint > 90){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 0.90f, transform.position.y, 0);
		}
		//90-100
		if(lifePoint == 90){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 0.75f, transform.position.y, 0);
		}
		//80-90
		if(lifePoint == 80){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 0.60f, transform.position.y, 0);
		}
		//70-80
		if(lifePoint == 70){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 0.45f, transform.position.y, 0);
		}
		//60-70
		if(lifePoint == 60){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 0.20f, transform.position.y, 0);
		}
		//50-60
		if(lifePoint == 50){
			transform.GetChild(0).position = new Vector3 (transform.position.x + 5, transform.position.y, 0);
		}
		//40-50
		if(lifePoint == 40){
			transform.GetChild(0).position = new Vector3 (transform.position.x + -0.47f,  transform.position.y, 0);
		}
		//30-40
		if(lifePoint == 30){
			transform.GetChild(0).position = new Vector3 (transform.position.x + -0.94f,  transform.position.y, 0);
		}
		//20-30
		if(lifePoint == 20){
			transform.GetChild(0).position = new Vector3 (transform.position.x + -1.41f,  transform.position.y, 0);
		}
		//10-20
		if(lifePoint == 10){
			transform.GetChild(0).position = new Vector3 (transform.position.x + -1.88f,  transform.position.y, 0);
		}
		//00-10
		if(lifePoint == 0){
			transform.GetChild(0).position = new Vector3 (transform.position.x + -2.35f,  transform.position.y, 0);
		}		
	}
}
