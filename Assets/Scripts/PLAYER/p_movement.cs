﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class p_movement : MonoBehaviour {
	float xInput = 0, yInput = 0;
	Vector3 mousePos, mouseVector;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GetInput ();
		Movement ();
	}

	void GetInput () {
		xInput = Input.GetAxis ("Horizontal");
		yInput = Input.GetAxis ("Vertical");
	}
	bool CheckCollision(Vector2 newVect, Vector2 newDir){
			RaycastHit2D hit = Physics2D.Raycast(newVect,newDir,0.001f);
			return hit;
	}
	void Movement () {
		Vector3 tempPos = transform.position;
		GameObject player = GameObject.FindWithTag("Player");
        p_type playerScript = player.GetComponent<p_type>();
		transform.position += new Vector3 (xInput, yInput, 0) * playerScript.playerSPEED * Time.deltaTime; //move the player based on input captures
		//transform.position = tempPos;
		if(CheckCollision(this.gameObject.transform.GetChild(0).position,Vector2.up) ||
		CheckCollision(this.gameObject.transform.GetChild(1).position,Vector2.down) &&
		CheckCollision(this.gameObject.transform.GetChild(2).position,Vector2.left) ||
		CheckCollision(this.gameObject.transform.GetChild(3).position,Vector2.right)){
			//speed = 0;
			transform.position = tempPos;
		}
		else {
			if (CheckCollision(this.gameObject.transform.GetChild(0).position,Vector2.up) ||
				CheckCollision(this.gameObject.transform.GetChild(1).position,Vector2.down)) {
				transform.position = new Vector3(gameObject.transform.position.x, tempPos.y, gameObject.transform.position.z);
			}
			else if(CheckCollision(this.gameObject.transform.GetChild(3).position,Vector2.right) ||
				CheckCollision(this.gameObject.transform.GetChild(2).position,Vector2.left)) {
					transform.position = new Vector3(tempPos.x, gameObject.transform.position.y, gameObject.transform.position.z);
				}
		}
		
	}
}
