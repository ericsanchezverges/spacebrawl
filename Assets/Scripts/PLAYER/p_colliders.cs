﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class p_colliders : MonoBehaviour {

	public Image keyE;
	//public Collider2D col;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	}
	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "enemyBullet"){
			GameObject player = GameObject.FindWithTag("Player");
        	p_type playerScript = player.GetComponent<p_type>();
			playerScript.playerHP -= 5.0f;
			Destroy(col.gameObject);
		}
	}

	void OnTriggerStay2D(Collider2D col) {
		GameObject player = GameObject.FindWithTag("Player");
        p_type playerScript = player.GetComponent<p_type>();
		p_shooting playerShoot = player.GetComponent<p_shooting>();
		
				
		if(col.gameObject.tag == "A_heal"){
			if(playerScript.playerHP < 100){
			playerScript.playerHP += 1.0f;
			}else{
				playerScript.playerHP = 100;
			}
		}
		if(col.gameObject.tag == "gunDrop"){
			keyE.enabled = true;
			
			Debug.Log("bones");
		}
		else{
			keyE.enabled = false;
		}
	}
}