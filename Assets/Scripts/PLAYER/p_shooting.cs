﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class p_shooting : MonoBehaviour {

	Vector3 mousePos, mouseVector;

	//used on shooting
	bool mouseLeft, canShoot;
	
	public GameObject bulletPrefab;
	public Transform gunSprite, gunTip;

	public enum typeOfWeapon{shotgun, machinegun, gun, laser};

	public typeOfWeapon myTypeOfWeapon;

	public int gunType;
	float 	lastShot = 0,
			timeBetweenShots = 0.5f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GetMouseInput();
		Shooting();
		Animation();
		if(Input.GetKeyDown(KeyCode.Alpha1)){
			gunType = 0;
		}else if(Input.GetKeyDown(KeyCode.Alpha2)){
			gunType = 1;
		}
	}

	void GetMouseInput () {
		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //position of cursor in world
		mousePos.z = transform.position.z; //keep the z position consistant, since we're in 2d
		mouseVector = (mousePos - transform.position).normalized; //normalized vector from player pointing to cursor
		mouseLeft = Input.GetMouseButton (0); //check left mouse button
	}
	void Shooting () {
		switch(gunType){
			case 1:
			ShotgunShooting();
			break;
			case 0:
			MachineGunShooting();
			break;
		}
	}
	
	void Animation () {
		float gunAngle = -1 * Mathf.Atan2 (mouseVector.y, mouseVector.x) * Mathf.Rad2Deg; //find angle in degrees from player to cursor
		gunSprite.rotation = Quaternion.Euler (0, 0, -gunAngle); //rotate gun sprite around that angle
	}

	void ShotgunShooting(){
		timeBetweenShots = 0.5f;
			canShoot = (lastShot + timeBetweenShots < Time.time);
			if (mouseLeft && canShoot) { //shoot if the mouse button is held and its been enough time since last shot
				Vector3 spawnPos = gunTip.position; //position of the tip of the gun, a transform that is a child of rotating gun
				Quaternion spawnRot = gunTip.transform.rotation; //no rotation, bullets here are round

					float angle = Mathf.Atan(mouseVector.y/mouseVector.x);

					Bullet bul = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul.Setup ( new Vector3(mouseVector.x ,mouseVector.y ,0)); //give the bullet a direction to fly

					if(mouseVector.x > 0){
					Bullet bul2 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul2.Setup ( new Vector3(Mathf.Cos(angle+0.3f),Mathf.Sin(angle+0.3f) ,0)); //give the bullet a direction to fly


					Bullet bul3 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul3.Setup ( new Vector3(Mathf.Cos(angle-0.3f),Mathf.Sin(angle-0.3f) ,0)); //give the bullet a direction to fly

					Bullet bul4 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul4.Setup ( new Vector3(Mathf.Cos(angle+0.15f),Mathf.Sin(angle+0.15f) ,0)); //give the bullet a direction to fly

					Bullet bul5 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul5.Setup ( new Vector3(Mathf.Cos(angle-0.15f),Mathf.Sin(angle-0.15f) ,0)); //give the bullet a direction to fly
					}
					else{

					Bullet bul2 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul2.Setup ( new Vector3(-Mathf.Cos(angle+0.3f),-Mathf.Sin(angle+0.3f) ,0)); //give the bullet a direction to fly

					Bullet bul3 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul3.Setup ( new Vector3(-Mathf.Cos(angle-0.3f),-Mathf.Sin(angle-0.3f) ,0)); //give the bullet a direction to fly

					Bullet bul4 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul4.Setup ( new Vector3(-Mathf.Cos(angle+0.15f),-Mathf.Sin(angle+0.15f) ,0)); //give the bullet a direction to fly

					Bullet bul5 = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
					bul5.Setup ( new Vector3(-Mathf.Cos(angle-0.15f),-Mathf.Sin(angle-0.15f) ,0)); //give the bullet a direction to fly
					}
				lastShot = Time.time; //used to check next time this is called
		}
	}

	void MachineGunShooting(){
		 timeBetweenShots = 0.1f;
		canShoot = (lastShot + timeBetweenShots < Time.time);
		if (mouseLeft && canShoot) { //shoot if the mouse button is held and its been enough time since last shot
			Vector3 spawnPos = gunTip.position; //position of the tip of the gun, a transform that is a child of rotating gun
			Quaternion spawnRot = gunTip.transform.rotation; 
			Bullet bul = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
			bul.Setup (mouseVector); //give the bullet a direction to fly
			lastShot = Time.time; //used to check next time this is called
		}
	}
}
