﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sc_Objects : MonoBehaviour {

	public bool objectON = false;
	public GameObject player;

	//invisible
	public GameObject F_invisible;
	float cooldownInvisible = 5f;
	public bool invisibleON = false;
	

	//shield
	public GameObject F_shield;
	float cooldownShield = 5f;
	public GameObject O_shield;		
	GameObject shieldObject;	
	public bool shieldON = false;
	

	//poison
	public GameObject F_poison;
	float cooldownPoison = 5f;
	public GameObject poisonPrefab;
	GameObject poisonCreate;
	public bool poisonON = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		checkObjects();
		Invisibility();	
	}

	void Invisibility(){
		Color tmp = player.GetComponent<SpriteRenderer>().color;
		if(invisibleON){
 			tmp.a = 0.3f;	
		}else{
 			tmp.a = 1f;
		}
		player.GetComponent<SpriteRenderer>().color = tmp;
	}

	void checkObjects(){
		if(objectON){
			if(invisibleON){
				if(cooldownInvisible >= 0){
					cooldownInvisible -= Time.deltaTime;		
				}else if(cooldownInvisible <= 0){
					invisibleON = false;
					objectON = false;
					cooldownInvisible = 5f;
				}
			}
			if(shieldON){
				if(cooldownShield >= 0){
					cooldownShield -= Time.deltaTime;		
				}else if(cooldownShield <= 0){
					shieldON = false;
					objectON = false;
					cooldownShield = 5f;
					Destroy(shieldObject);
				}
			}
			if(poisonON){
				if(cooldownPoison >= 0){
					cooldownPoison -= Time.deltaTime;		
				}else if(cooldownPoison <= 0){
					poisonON = false;
					//objectON = false;
					cooldownPoison = 5f;
					Destroy(poisonCreate);
				}
			}
		}
	}


	private void OnTriggerEnter2D(Collider2D other) {
		if(!objectON){
			if (other.tag == "F_shield"){
				Destroy(other.gameObject);
				shieldObject = Instantiate (O_shield, new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
				shieldObject.transform.parent = player.transform;
				shieldON = true;
				objectON = true;
			}
			if (other.tag == "F_invisible"){
				Destroy(other.gameObject);
				invisibleON = true;
				objectON = true;
			}
		}
		if (other.tag == "F_poison"){
			Destroy(other.gameObject);
			poisonCreate = Instantiate(poisonPrefab, other.gameObject.transform.position, Quaternion.identity) as GameObject;
			poisonON = true;
			//objectON = true;
		}		
	}	
}
