﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class energyBarHandler : MonoBehaviour {
	public Image energyBar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject player = GameObject.FindWithTag("Player");
        p_type playerScript = player.GetComponent<p_type>();
		energyBar.fillAmount = 1.0f-playerScript.cooldownAbility/10;
	}
}
