﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class p_type : MonoBehaviour {

	public int playerType = 1;
	public float cooldownAbility;
	Vector3 playerPOS;
	public GameObject player;
	public Sprite tank, dasher, healer, fury;
	public float playerHP = 100.0f;
	public float playerSPEED = 20.0f;
	public float playerDMG = 200.0f;

	//mouse
	Vector3 mousePos, mouseVector;
	public Transform gunSprite;

	//furyType variables
	bool checkfury= false;
	float furyDMG = 20.0f;
	float furySPEED = 5.0f;
	//healerType variables
	bool checkHeal = false;
	public GameObject healPrefab;
	GameObject healCreate;
	//tankType variables
	bool checkShield = false;
	public GameObject shieldPrefab;
	GameObject shieldCreate;
	//dasherType variables
	float playerSpeed;
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		UpdateCooldown();
		useAbility();
		SetClass(playerType);
		if(playerHP <= 0){
			SceneManager.LoadScene(2);
		}
	}

	void mouseInput(){
		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //position of cursor in world
		mousePos.z = transform.position.z; //keep the z position consistant, since we're in 2d
		mouseVector = (mousePos - transform.position).normalized; //normalized vector from player pointing to cursor
	}

	void useAbility(){
		if (Input.GetKeyDown ("space")) {
			charAbility (playerType);				
		}
		if (Input.GetKeyDown ("q")) {
			if (playerType != 4) playerType += 1;	
			else playerType = 1;			
		}
	}

	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
	void charAbility (int chType) {		
		//TANK
		if (chType == 1) {
				if (!checkShield) {
					if(cooldownAbility <= 0){
						playerPOS = transform.position;
						shieldCreate = Instantiate (shieldPrefab, new Vector3 (playerPOS.x, playerPOS.y, playerPOS.z), gunSprite.rotation) as GameObject;
						shieldCreate.transform.parent = gunSprite.transform;
						checkShield = true;
						cooldownAbility = 3.0f;
					}
				} else if (checkShield) {
					Destroy (shieldCreate);
					checkShield = false;	
				}	
		}
		//HEALER
		if (chType == 2) {
			if (!checkHeal) {
				if(cooldownAbility <= 0){
					healCreate = Instantiate (healPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
					healCreate.transform.parent = player.transform;
					checkHeal = true;
					cooldownAbility = 3.0f;
				}
			} else if (checkHeal) {
				Destroy (healCreate);
				checkHeal = false;
			}
		}
		//FURY
		if (chType == 3) {
			if (!checkfury) {
				if(cooldownAbility <= 0){			
					playerDMG += furyDMG;
					playerSPEED += furySPEED;
					checkfury = true;
					cooldownAbility = 3.0f;
				}
			} else if (checkfury) {
				playerDMG -= furyDMG;
				playerSPEED -= furySPEED;
				checkfury = false;
			}
		}
		//DASHER
		if (chType == 4) {
			if(cooldownAbility <= 0){
				transform.position += new Vector3 (playerSpeed * Time.deltaTime, 0f, 0.0f) + mouseVector;
				cooldownAbility = 1.0f;
			}
		}
	}

	void SetClass (int chType) {
		if (chType == 1) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = tank; 
		}
		if (chType == 2) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = healer;
		}
		if (chType == 3) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = fury;
		}
		if (chType == 4) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = dasher;
		}
	}
}
