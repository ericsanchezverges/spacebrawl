﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	Vector3 dir, nextDir;
	float speed, speedDecay = 1f, minSpeed = 0.1f, startSpeed = 50;
	bool recalling, disappearing;
	Transform recallTarget;
	public SpriteRenderer bulletRend;
	float wTime;
	public void Setup (Vector3 _dir) { 
		dir = _dir; //passed in from player
		speed = startSpeed; //start moving
	}
	void FixedUpdate () {
		Move(); //move the bullet
		//CheckDisappear(); //get rid of bullet after it stops moving
		waitTime();
	}

	void waitTime(){
		wTime += Time.deltaTime;
	
	}
	void Move(){
		//speed = Mathf.Lerp(speed, 0f, 0.8f * Time.deltaTime);
		speed += speedDecay * speed * Time.fixedDeltaTime; //slow down the bullet over time
		if (speed < minSpeed){
			speed = 0; //clamp down speed so it doesnt take too long to stop
		}
		Vector3 tempPos = transform.position; //capture current position
		tempPos += dir * speed * Time.fixedDeltaTime; //find new position
		transform.position = tempPos; //update position
	}
	
	
	private void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("Wall")){
		Destroy(gameObject);
		}

	}
}