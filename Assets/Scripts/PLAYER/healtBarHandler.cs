﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class healtBarHandler : MonoBehaviour {
	public Image healthBar;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		GameObject player = GameObject.FindWithTag("Player");
        p_type playerScript = player.GetComponent<p_type>();
		healthBar.fillAmount = playerScript.playerHP/100;
	}
}
