﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;

public class timerScript : MonoBehaviour {

	public Text timeText;
	public Text roundText;
 	public float time = 10.0f;
	public int round = 1;

	//spawner
	public GameObject enemySpawner;
	Vector3 randPos;
	bool enemiesKilled = true;

 	public void Update(){
		Rounds();
		TimerFunct();
 	}

	void Rounds(){
		roundText.text = "Round:" + round.ToString("f0");
	}
	
	void TimerFunct(){
		countDown();
		timeText.text = "Time:" + time.ToString("f0");	
		if(time<=0 && enemiesKilled){
			spawSpawner();
			time = 10f;
			round++;
			//enemiesKilled = false;
		}else if(time<=0 && !enemiesKilled){
			time = 0f;
		}
	}

	void countDown(){
		time = time - 1 * Time.deltaTime;
	}
	void countUp(){
		time = time + 1 * Time.deltaTime;
	}

	void spawSpawner(){
		randPos = new Vector3(Random.Range(transform.position.x-20, transform.position.x+20), Random.Range(transform.position.y-20,transform.position.y+20),0);
		Instantiate (enemySpawner, randPos, Quaternion.identity);
	}
}
