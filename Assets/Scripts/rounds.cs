﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class rounds : MonoBehaviour {

	public int round = 1;
	public int i = 0;
	Vector3 randPos;
	public GameObject spawner;
	public float cooldown = 5f;
	


	// Use this for initialization
	void Start () {
		//enemyCreate();
	}

	void enemyCreate(){
		randPos = new Vector3(Random.Range(transform.position.x-20, transform.position.x+20), Random.Range(transform.position.y-20,transform.position.y+20),0);
		GameObject tempSpawner = Instantiate (spawner, randPos, Quaternion.identity);
	}
	
	// Update is called once per frame
	void Update () {
		changeRound();
		GameObject player = GameObject.FindWithTag("Player");
		gameObject.transform.position = player.transform.position;
	}

	void changeRound(){
			if(cooldown <= 0){
				round++;
				enemyCreate();
				cooldown = 5.0f;
			}
	}

	void UpdateCooldown(){
		if(cooldown >= 0){
			cooldown -= Time.deltaTime;		
		}
	}

}
