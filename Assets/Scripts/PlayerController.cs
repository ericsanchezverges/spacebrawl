﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
	/*float xInput = 0, yInput = 0;*/
	bool mouseLeft, canShoot;
	float lastShot = 0, timeBetweenShots = 0.1f;
	Vector3 mousePos, mouseVector;
	public Image HealthBar;
	public Vector3 playerPosition;
	public Quaternion playerRotation;
	public Transform gunSprite, gunTip;
	public GameObject player;
	public GameObject bulletPrefab;
	public GameObject healPrefab;
	public GameObject shieldPrefab;
	GameObject shieldCreate, healCreate;
	public int charType = 0;
	public int hp, speed, dmg = 0;
	public int checkShield, checkfury, checkHeal = 0;
	
	public Sprite tank, dasher, healer, fury;

	void Start () {
		//GetMouseInput ();
		hp = 100;
		SetClass (charType);
	}
	void Update () {
		GetInput (); //capture wasd and mouset
		//Movement (); //move the player
		Animation (); //rotate the gun
		Shooting (); //handle shooting
		ObjectButton();
		UpdateCooldown();
		UpdateHpBar();
	}
	void GetInput () {
		/*xInput = Input.GetAxis ("Horizontal");
		yInput = Input.GetAxis ("Vertical"); //capture wasd and arrow controls*/
		//GetMouseInput ();
	}


	float cooldownAbility = 0.0f;
	float cooldownBase = 3.0f;
	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
	void ObjectButton(){
		if (Input.GetKeyDown ("q")) {
			if (charType != 4) charType++;
			else charType = 1;
			//Debug.Log (charType);
			SetClass (charType);
		}
		if (Input.GetKeyDown ("space")) {
			charAbility (charType);				
		}
	}

	void charAbility (int chType) {
		if (chType == 1) {
				if (checkShield == 0) {
					if(cooldownAbility <= 0){
						playerPosition = transform.position;
						playerRotation = transform.rotation;
						shieldCreate = Instantiate (shieldPrefab, new Vector3 (playerPosition.x, playerPosition.y, playerPosition.z) + mouseVector * 2, gunSprite.rotation) as GameObject;
						shieldCreate.transform.parent = gunSprite.transform;
						checkShield = 1;
						cooldownAbility = cooldownBase;
					}
				} else if (checkShield == 1) {
					Destroy (shieldCreate);
					checkShield = 0;	
				}	
		}
		if (chType == 2) {
			if (checkHeal == 0) {
				if(cooldownAbility <= 0){
					playerPosition = transform.position;
					playerRotation = transform.rotation;
					healCreate = Instantiate (healPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z), playerRotation) as GameObject;
					healCreate.transform.parent = player.transform;
					checkHeal = 1;
					cooldownAbility = cooldownBase;
				}
			} else if (checkHeal == 1) {
				Destroy (healCreate);
				checkHeal = 0;
			}
		}
		if (chType == 3) {
			if (checkfury == 0) {
				if(cooldownAbility <= 0){
					dmg += 20;
					speed += 4;
					checkfury = 1;
					cooldownAbility = cooldownBase;
				}
			} else if (checkfury == 1) {
				dmg = 0;
				speed -= 4;
				checkfury = 0;
			}
		}
		if (chType == 4) {
			if(cooldownAbility <= 0){
				transform.position += new Vector3 (speed * Time.deltaTime, 0f, 0.0f) + mouseVector * 5;
				cooldownAbility = 1.0f;
			}
		}
	}

	void GetMouseInput () {
		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition); //position of cursor in world
		mousePos.z = transform.position.z; //keep the z position consistant, since we're in 2d
		mouseVector = (mousePos - transform.position).normalized; //normalized vector from player pointing to cursor
		mouseLeft = Input.GetMouseButton (0); //check left mouse button
	}

	void Animation () {
		float gunAngle = -1 * Mathf.Atan2 (mouseVector.y, mouseVector.x) * Mathf.Rad2Deg; //find angle in degrees from player to cursor
		gunSprite.rotation = Quaternion.Euler (0, 0, -gunAngle); //rotate gun sprite around that angle
	}
	void Shooting () {
		canShoot = (lastShot + timeBetweenShots < Time.time);
		if (mouseLeft && canShoot) { //shoot if the mouse button is held and its been enough time since last shot
			Vector3 spawnPos = gunTip.position; //position of the tip of the gun, a transform that is a child of rotating gun
			Quaternion spawnRot = Quaternion.identity; //no rotation, bullets here are round
			Bullet bul = Instantiate (bulletPrefab, spawnPos, spawnRot).GetComponent<Bullet> (); //spawn bullet and capture it's script
			bul.Setup (mouseVector); //give the bullet a direction to fly
			lastShot = Time.time; //used to check next time this is called
		}
	}

	/*
	void Movement () {
		Vector3 tempPos = transform.position;
		transform.position += new Vector3 (xInput, yInput, 0) * speed * Time.deltaTime; //move the player based on input captures
		//transform.position = tempPos;
		if(CheckCollision(this.gameObject.transform.GetChild(0).position,Vector2.up) || CheckCollision(this.gameObject.transform.GetChild(4).position,Vector2.up) ||
		CheckCollision(this.gameObject.transform.GetChild(1).position,Vector2.down) || CheckCollision(this.gameObject.transform.GetChild(5).position,Vector2.down) &&
		CheckCollision(this.gameObject.transform.GetChild(2).position,Vector2.left) || CheckCollision(this.gameObject.transform.GetChild(6).position,Vector2.left) ||
		CheckCollision(this.gameObject.transform.GetChild(3).position,Vector2.right) || CheckCollision(this.gameObject.transform.GetChild(7).position,Vector2.right)){
			//speed = 0;
			transform.position = tempPos;
		}
		else {
			if (CheckCollision(this.gameObject.transform.GetChild(0).position,Vector2.up) || CheckCollision(this.gameObject.transform.GetChild(4).position,Vector2.up) ||
				CheckCollision(this.gameObject.transform.GetChild(1).position,Vector2.down) || CheckCollision(this.gameObject.transform.GetChild(5).position,Vector2.down)) {
				transform.position = new Vector3(gameObject.transform.position.x, tempPos.y, gameObject.transform.position.z);
			}
			else if(CheckCollision(this.gameObject.transform.GetChild(3).position,Vector2.right) || CheckCollision(this.gameObject.transform.GetChild(7).position,Vector2.right) ||
				CheckCollision(this.gameObject.transform.GetChild(2).position,Vector2.left) || CheckCollision(this.gameObject.transform.GetChild(6).position,Vector2.left)) {
					transform.position = new Vector3(tempPos.x, gameObject.transform.position.y, gameObject.transform.position.z);
				}
		}
		
	}
*/
	
	/* 
	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "enemyBullet"){
			hp = hp - 1;
			Destroy(col.gameObject);
		}
	}

	void OnTriggerStay2D(Collider2D col) {
		if(col.gameObject.tag == "A_heal"){
			hp = hp + 5;
		}
	}
	*/
	

	bool CheckCollision(Vector2 newVect, Vector2 newDir){
			RaycastHit2D hit = Physics2D.Raycast(newVect,newDir,0.001f);
			return hit;
	}

	void SetClass (int chType) {

		int tankHP = 100;
		int healerHP = 100;
		int furyHP = 100;
		int dasherHP = 100;

		if (chType == 1) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = tank; 
			hp = tankHP;
			if(hp>tankHP) hp = tankHP;
			speed = 8;
		}
		if (chType == 2) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = healer;
			hp = healerHP;
			if(hp>healerHP) hp = healerHP;
			speed = 10;
		}
		if (chType == 3) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = fury;
			hp = furyHP;
			if(hp>furyHP) hp = furyHP;
			speed = 10;
		}
		if (chType == 4) {
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = dasher;
			hp = dasherHP;
			if(hp>dasherHP) hp = dasherHP;
			speed = 12;
		}
	}
	void UpdateHpBar(){
		HealthBar.fillAmount = hp/100;
	}
}

	