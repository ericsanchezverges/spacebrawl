﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class enemy : MonoBehaviour {

	//public Transform target;
	public GameObject wall;
	int speed = 0;
	public int enemyHP = 50;
	Transform SpawnPoint;
	public GameObject enemyBulletPrefab;
	float cooldownAbility = 0.0f;
	private Vector3 posPlayer;
	public GameObject enemyPrefab;
	public float distance;
	public Text enemyHPtext;
	public bool shielded = false;

	//enemyType
	public int enemyType = 1; //0-shooter 1-bomb 2-divide
	int distanceByType;
	public Sprite enemyShootSpr, enemyBombSpr, enemyDivideSpr;

	//enemyBomb
	public GameObject bombPrefab;

	//enemyDivide
	public GameObject dividePrefab;
	Vector3 randpos1;
	Vector3 randpos2;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		posPlayer = GameObject.FindWithTag("Player").transform.position;
		enemyTypeFunct();
		getInvisible();
		UpdateCooldown();
		enemyDead();
		enemyHPtext.text = "" + enemyHP.ToString("f0");	
	}

	//enemy movement
	void MoveEnemy(){
		distance = Vector3.Distance(transform.position,posPlayer);
		if(distance > distanceByType){
			transform.position = Vector3.MoveTowards(transform.position, posPlayer, speed*Time.deltaTime);
		}	
	}	
	

	//enemy shooting
	void ShootEnemy(){
		if(distance < 10){
				if(cooldownAbility <= 0){
					enemyBullet enemyBullet = (Instantiate (enemyBulletPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z),transform.rotation) as GameObject).GetComponent<enemyBullet>();
					cooldownAbility = 2.0f;
				}
		}
	}

	//enemy dying
	void enemyDead(){
		if(enemyHP <= 0){
			Destroy(this.gameObject);
			if(enemyType == 1){
				GameObject bomb = Instantiate (bombPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
			}if(enemyType==2){
				randpos1 = new Vector3(Random.Range(transform.position.x-5, transform.position.x+5), Random.Range(transform.position.y-5,transform.position.y+5),0);
				GameObject divide = Instantiate (dividePrefab, randpos1, transform.rotation) as GameObject;
				randpos2 = new Vector3(Random.Range(transform.position.x-5, transform.position.x+5), Random.Range(transform.position.y-5,transform.position.y+5),0);
				divide = Instantiate (dividePrefab, randpos2, transform.rotation) as GameObject;
			}
		}
		if(distance<=2 && enemyType==1){
			Destroy(gameObject);
			GameObject bomb = Instantiate (bombPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z), transform.rotation) as GameObject;
		}
	}

	//colliders
	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "playerBullet"){
			enemyHP = enemyHP - 10;
			Destroy(col.gameObject);
		}
	}
	void OnTriggerStay2D (Collider2D col) {
		if(col.gameObject.tag == "Poison"){
			enemyHP = enemyHP - 1;
		}
	}
	bool CheckCollision(Vector2 newVect, Vector2 newDir){
			RaycastHit2D hit = Physics2D.Raycast(newVect,newDir,0.001f);
			return hit;
	}

	//get if player is invisible
	void getInvisible(){
		GameObject player = GameObject.FindWithTag("Player");
        sc_Objects invisibleState = player.GetComponent<sc_Objects>();
		if(!invisibleState.invisibleON){
			MoveEnemy();
			if(enemyType==0){
				ShootEnemy();
			}
			
		}
	}

	//defines the type of the enemy
	void enemyTypeFunct(){
		if(enemyType==0){ //shooter
			distanceByType = 10;
			speed = 5;
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = enemyShootSpr; 
		}
		if(enemyType==1){ //bomber
		speed = 10;
			distanceByType = 1;
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = enemyBombSpr; 
		}
		if(enemyType==2){ //divide
		speed = 3;
			distanceByType = 5;
			this.gameObject.GetComponent<SpriteRenderer> ().sprite = enemyDivideSpr; 
		}
	}

	//cooldown
	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
}
