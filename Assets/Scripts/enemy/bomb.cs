﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class bomb : MonoBehaviour {

	public Text timeExplode;
	float eTime = 0.5f;
	float time = 0.8f;
	Vector3 posPlayer;
	float distance;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		posPlayer = GameObject.FindWithTag("Player").transform.position;
		distance = Vector3.Distance(transform.position,posPlayer);
		TimerFunct();
	}

	void countDown(){
		time = time - 1 * Time.deltaTime;
	}

	void TimerFunct(){
		countDown();
		timeExplode.text = "" + time.ToString("f0");	
		if(time<=0){
			if(distance < 4){
				GameObject player = GameObject.FindWithTag("Player");
        		p_type HPplayer = player.GetComponent<p_type>();
				HPplayer.playerHP -= 20;
				Destroy(gameObject);
			}
			Destroy(gameObject);
		}
	}
}
