﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBullet : MonoBehaviour {


	float bulletSpeed = 25f;
	private Vector3 posPlayer;
	private Vector3 posPlayerNow;
	Vector3 dir, bulletVector;
	private float playerX;
	private float playerY;
	public void Direction (Vector3 _dir) { 
		dir = _dir; //passed in from player
	}

	// Use this for initialization
	void Start () { 		 	
		//posPlayer = GameObject.FindWithTag("Player").transform.position;
		Destroy(gameObject, 1);
		posPlayer = GameObject.FindWithTag("Player").transform.position;
		bulletVector = (posPlayer - transform.position).normalized;	 

	}
	
	// Update is called once per frame
	void Update () {		
		MoveBullet();
		
	}
	void MoveBullet(){
		transform.position += bulletVector * Time.deltaTime * bulletSpeed;
	}
}
