﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class enemyDouble : MonoBehaviour {

	//public Transform target;
	public GameObject wall;
	int speed = 8;
	public int enemyHP = 30;
	Transform SpawnPoint;
	float cooldownAbility = 0.0f;
	private Vector3 posPlayer;
	public float distance;
	public Text enemyHPtext;
	enemyBullet enemyBullet;
	public GameObject enemyBulletPrefab;
	Quaternion enemyRotation;

	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		posPlayer = GameObject.FindWithTag("Player").transform.position;
		MoveEnemy();
		ShootEnemy();
		UpdateCooldown();
		enemyDead();
		enemyHPtext.text = "" + enemyHP.ToString("f0");	
	}

	void enemyDead(){
		if(enemyHP <= 0){
			Destroy(this.gameObject);
		}
	}

	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "playerBullet"){
			enemyHP = enemyHP - 10;
			Destroy(col.gameObject);
		}
	}

	void OnTriggerStay2D (Collider2D col) {
		if(col.gameObject.tag == "Poison"){
			enemyHP = enemyHP - 1;
		}
	}

	bool CheckCollision(Vector2 newVect, Vector2 newDir){
			RaycastHit2D hit = Physics2D.Raycast(newVect,newDir,0.001f);
			return hit;
	}

	/*void getInvisible(){
		GameObject player = GameObject.FindWithTag("Player");
        sc_Objects invisibleState = player.GetComponent<sc_Objects>();
		if(!invisibleState.invisibleON){
			MoveEnemy();
			ShootEnemy();
		}
	}*/

	void MoveEnemy(){
		distance = Vector3.Distance(transform.position,posPlayer);
		if(distance > 10){
			transform.position = Vector3.MoveTowards(transform.position, posPlayer, speed*Time.deltaTime);
		}
	}

	void ShootEnemy(){
		if(distance < 15){
				if(cooldownAbility <= 0){
					enemyBullet = (Instantiate (enemyBulletPrefab, new Vector3 (transform.position.x, transform.position.y, transform.position.z), enemyRotation) as GameObject).GetComponent<enemyBullet>();
					cooldownAbility = 2.0f;
				}
		}
	}

	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
}