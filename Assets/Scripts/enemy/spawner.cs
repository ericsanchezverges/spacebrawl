﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {
	public GameObject enemy;
	public GameObject enemyShielder;
	public GameObject spawnerPrefab;
	float cooldownAbility;
	public int spawnerType = 0;
	int randRange;
	



	// Use this for initialization
	void Start () {
		randRange = Random.Range(0,3);
	}
	
	// Update is called once per frame
	void Update () {
		spawnenemy();
		UpdateCooldown();
	}

	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
	void spawnenemy(){
		//randPosEnemy = new Vector3(Random.Range(transform.position.x-20, transform.position.x+20), Random.Range(transform.position.y-20,transform.position.y+20),0);
		spawnerType = randRange;
		if(cooldownAbility <= 0){				
			GameObject tempEnemy = Instantiate (enemy, this.gameObject.transform.position, Quaternion.identity);
			if(spawnerType == 0){
				Instantiate (enemyShielder, this.gameObject.transform.position, Quaternion.identity);	
				tempEnemy.GetComponent<enemy>().enemyType = 0;
			}
			if(spawnerType == 1){
				tempEnemy.GetComponent<enemy>().enemyType = 1;
			}
			if(spawnerType == 2){
				tempEnemy.GetComponent<enemy>().enemyType = 2;
			}
			cooldownAbility = 10.0f;
		}
	}	

	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "playerBullet"){
			Destroy(col.gameObject);
			Destroy(gameObject);
		}
		if(col.gameObject.tag == "Wall"){
			GameObject tempSpawner = Instantiate (spawnerPrefab, new Vector3(Random.Range(transform.position.x-20, transform.position.x+20), Random.Range(transform.position.y-20,transform.position.y+20),0), Quaternion.identity);
			Destroy(gameObject);
		}
	}
}
