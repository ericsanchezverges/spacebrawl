﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class enemyShielder : MonoBehaviour {

	//public Transform target;
	public GameObject wall;
	int speed = 8;
	public int enemyHP = 30;
	Transform SpawnPoint;
	float cooldownAbility = 0.0f;
	private Vector3 nearestEnemy;
	bool nearestEnemyHere;
	GameObject nearestEnemyGO;
	public float distance;
	public Text enemyHPtext;
	Quaternion enemyRotation;
	GameObject shield;
	public GameObject shieldPrefab;
	bool shieldON = false;

	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		nearestEnemyHere = GameObject.FindWithTag("enemy");
		nearestEnemyGO = GameObject.FindWithTag("enemy");
		if(nearestEnemyHere){
			nearestEnemy = GameObject.FindWithTag("enemy").transform.position;
		}
		MoveEnemy();
		UpdateCooldown();
		enemyDead();
		enemyHPtext.text = "" + enemyHP.ToString("f0");	
	}

	void enemyDead(){
		if(enemyHP <= 0){
			Destroy(this.gameObject);
		}
	}

	void OnTriggerEnter2D (Collider2D col) {
		if(col.gameObject.tag == "playerBullet"){
			enemyHP = enemyHP - 10;
			Destroy(col.gameObject);
		}
	}

	void OnTriggerStay2D (Collider2D col) {
		if(col.gameObject.tag == "Poison"){
			enemyHP = enemyHP - 1;
		}
	}

	bool CheckCollision(Vector2 newVect, Vector2 newDir){
			RaycastHit2D hit = Physics2D.Raycast(newVect,newDir,0.001f);
			return hit;
	}

	void MoveEnemy(){
		distance = Vector3.Distance(transform.position,nearestEnemy);
		if(nearestEnemyHere){
			if(distance >= 5){
				transform.position = Vector3.MoveTowards(transform.position, nearestEnemy, speed*Time.deltaTime);
			}if(distance <=5){
				shieldingEnemy();
			}
		}
		
	}

	void shieldingEnemy(){
		distance = Vector3.Distance(transform.position,nearestEnemy);
		if(!shieldON){
			if(distance <= 10 ){
				shield = Instantiate (shieldPrefab, nearestEnemy, transform.rotation) as GameObject;
				shield.transform.parent = nearestEnemyGO.transform;
				shieldON = true;
			}
		}else{
			if(enemyHP <= 0){
				Destroy(shield);
				shieldON = false;
			}
		}
		
	}
	
	void UpdateCooldown(){
		if(cooldownAbility >= 0){
			cooldownAbility -= Time.deltaTime;		
		}
	}
}
